# Register usage: $3=n, $4=f1, $5=f2 $6:address storage
# return value written to address 128
.globl fibn
fibn:	addi $3, $0, 14		# initialize n = 14
	addi $4, $0, 1		# initialize f1 = 1
	addi $5, $0, -1		# initialize f2 = -1
	addi $6, $0, 128
loop:	beq $3, $0, end		# Done with loop if n = 0
	add $4, $4, $5		# f1 = f1 + f2
	sub $5, $4, $5		# f2 = f1 - f2
	addi $3, $3, -1		# n = n - 1
	sb $4, 0($6)		# store result in address after 128
	addi $6, $6, 1
	j loop			# repeat until done
end:  lb $1, 255($0) #load switch value
  addi $2, $1, 128  #add 128 to switch location
  lb $3, 0($2)      #load value at 128+switch
  sb $3, 255($0)    #output value to leds
  j end

