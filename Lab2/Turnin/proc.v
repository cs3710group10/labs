`timescale 1ns / 1ps
module proc #(parameter WIDTH = 8)
				(input clk, reset,
				input 		[7:0]			swinput,
				output reg 	[7:0]			ledoutput);
	wire [WIDTH-1:0] memdata, adr, writedata;
	reg [WIDTH-1:0] indata = 0;
	
	wire memread,memwrite;
	
	mips #(WIDTH) mproc(clk,reset,indata,memread,memwrite,adr,writedata);
	
	
	exmem #(WIDTH) mem(~clk,memread|memwrite,memwrite,adr,writedata,memdata);
	
	always @(*) begin
		if(reset)
			indata = 8'h00;
		else if (adr[7:6] == 2'b11)
			indata = swinput;
		else
			indata = memdata;
	end
	always @(*) begin
		if (reset) begin
			ledoutput <= 8'h00;
		end else if ((adr & 8'b11000000) && memwrite) begin
			ledoutput <= writedata;
		end else begin
			ledoutput <= ledoutput;
		end
	end
endmodule