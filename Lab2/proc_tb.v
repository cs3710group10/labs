`timescale 1ns / 1ps
module proc_tb;
reg clk, reset;
reg [7:0] swinput;
wire [7:0] ledout;
proc #(8) p1(clk,reset,swinput,ledout);
 
initial begin
	swinput=0;
	clk=0;
	reset=1;
	#10;
	reset=0;
	while (1) begin
		swinput = swinput + 1;
		#497;
		if(swinput > 13)
			swinput = 2'h00;
		#3;
	end
end
always
	#5 clk = !clk;
	
endmodule