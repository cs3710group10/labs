transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display/vga_controller.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display/vga_bitgen.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display/vga.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display/glyph_memory.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display/charset_rom.v}

vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display/output_files {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display/output_files/vga_tb.v}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cyclonev_ver -L cyclonev_hssi_ver -L cyclonev_pcie_hip_ver -L rtl_work -L work -voptargs="+acc"  vga_tb

add wave *
view structure
view signals
run -all
