transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/sdcard {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/sdcard/spi.v}
vlog -vlog01compat -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/sdcard {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/sdcard/spi_fsm.v}

vlog -vlog01compat -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/sdcard {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/sdcard/spi_tb.v}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cyclonev_ver -L cyclonev_hssi_ver -L cyclonev_pcie_hip_ver -L rtl_work -L work -voptargs="+acc"  spi_tb

add wave *
view structure
view signals
run -all
