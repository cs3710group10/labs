transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/keyboard {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/keyboard/ps2ascii.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/keyboard {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/keyboard/keyboard_memory.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor/IOLogic.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor/datapath.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor/regfile.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor/processor.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor/pc.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor/mux_2to1.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor/FSM.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor/flags.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor/alu.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/keyboard {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/keyboard/ps2_in.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display/vga_controller.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display/vga_bitgen.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display/vga.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/FinalProject.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/io_interface.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display/glyph_memory.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/IO/display/charset_rom.v}
vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/Processor/memory.v}

vlog -sv -work work +incdir+/home/klemmon/Desktop/Fall2019/CS3710/FinalProject {/home/klemmon/Desktop/Fall2019/CS3710/FinalProject/tb_finalproject.v}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cyclonev_ver -L cyclonev_hssi_ver -L cyclonev_pcie_hip_ver -L rtl_work -L work -voptargs="+acc"  tb_finalproject

add wave *
view structure
view signals
run -all
