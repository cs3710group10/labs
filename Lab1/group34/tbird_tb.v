`timescale 1ns / 1ps
/* 
 * ECE 3710
 * Fall 2019
 * Lab 01
 * Group 10_34: Kyle Lemmon and Colton Watson
 * September 5, 2019
 *
 * Simple test bench for the thunderbird turn signal FSM.
 *
 */
module tbird_tb;
	reg LEFT, RIGHT, HAZ, clk, resetn;

	wire [2:0] L, R;

	integer i;
	
	tbird UUT (.LEFT(LEFT), .RIGHT(RIGHT), .HAZ(HAZ), .clk(clk), .resetn(resetn), .L(L), .R(R));
	
	initial begin
	
	// Set all values safetly.
	resetn = 1'b1;
	LEFT  = 1'b0;
   RIGHT = 1'b0;
	HAZ   = 1'b0;
	clk   = 1'b0;
	
	#10;
	
	resetn = 1'b0;
	
	#10;
	
	resetn = 1'b1;
	
	#10;
	
	$display("===Starting Simulation===");  
   
	// Run through the inputs.
	for(i = 0; i < 8; i = i + 1) begin
	   {HAZ, LEFT, RIGHT} = i;
		
		#40000000;
	
	   // Wait some large amount of time. During testing, we adjusted the enable clock 
	   // to pulse at a higher rate, making the simulation faster.	
	
	end
	

	$stop;
	end
	
	always
		# 5 clk = !clk;


endmodule
