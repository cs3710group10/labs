module lab1(LEFT,RIGHT,HAZ,clk,L,R);
  input RIGHT,LEFT,HAZ,clk;
  output reg [2:0] L,R;

  wire adjclk;

  int count = 0;
  reg [2:0] PS, NS;
  parameter DF=3'b000,
            L0=3'b001,
            L1=3'b010,
            L2=3'b011,
            R0=3'b100,
            R1=3'b101,
            R2=3'b110,
            HZ=3'b111;

  always @(posedge clk) begin
    if(count == 2**23) begin
      count = 0;
      adjclk = ~adjclk;
    end
    count = count + 1;
  end

  always @(posedge adjclk) begin
    case (PS)
      DF:begin
        if(LEFT)
          NS<=L0;
        if(RIGHT)
          NS<=R0;
        R = L = 3'b000;
      end
      L0:begin
        NS <= L1;
        L = 3'b001;
      end
      L1:begin
        NS<=L2;
        L = 3'b001;
      end
      L2:begin
        NS<=DF;
        L = 3'b001;
      end
      R0:begin
        NS<=R1;
        R = 3'b001;
      end
      R1:begin
        NS<=R2;
        R = 3'b011;
      end
      R2:begin
        NS<=DF;
        R = 3'b111;
      end
      HZ:begin
        if(~HAZ)
          NS<=DF;
        L = R = ~L;
      end
    endcase
    if(HAZ & (PS != HZ)) begin
      NS = HZ;
      L = R = 3'b111;
    end
  end
endmodule
