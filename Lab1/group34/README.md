#Lab 1, Thunderbird Turn Signals
##Results
The circuit works as expected both when simulated and when programmed onto the FPGA.

##Problems
The largest problems encountered had to do with remembering verilog syntax and conventions, and the operation of the simulation tools.
There was a failure to recognize the clock requirements, and we ended up having to re-write our clock divider into an enable signal.

##Included files
###Verilog
tbird.v     -- The Thunderbird turn signal circuit, including the state machine. 
tbird_tb.v  -- The Thunderbird turn signal testbench, which verifies the desired output of the turn signal circuit.
clken.v     -- The clock enable circuit, which serves to slow the state machine down by outputing an enable signal for one clock cycle, every 10 million cycles. Effectively a 20ns pulse every 200ms, given a 50Mhz clock.
###Report
Report.pdf  -- A detailed description of the design functionality of the state machine behind the turn signal circuit, as well as a description of the clock enable circuit, and a screen capture of the testbench waveform of the finished Thunderbird turn signal circuit.
Report.tex  -- A LaTex file used to write the report.
###Readme
README.md   -- This file, describing briefly the project results, problems and included files.

