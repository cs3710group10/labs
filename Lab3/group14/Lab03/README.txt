Lab 3, Group 10
Colton Watson and Yuntong Lu

In this project, we were able to have output VGA from our FPGA.
We decided to create two seperate folders for them, on for the colors example,
and one for the thunderbird example.

In the 'Colors' folder:
	1. bitgen.v - The bitgen for the colors example.
	2. bitgen_tb.v - Testbench for bitgen.
	3. vga.v - Top level module for the file.
	4. vgaControl.v - The VGA controller.
	
In the 'Thunderbird' folder:
	1. t_bird1.v - The thunderbird state machine.
	2. tb_VGA_top.v - Testbench for the module.
	3. VGA_Bitgen.v - An extra bit generator.
	4. VGA_Controller.v - VGA controller for FPGA.
	5. VGA_top.v - Top level module.
	