module VGA_Bitgen(input bright, 
			input [2:0] L, R,
			input clk,
			input [9:0] hCount, vCount, 
			output reg [20:0] rgb); 
			
	parameter BLACK = 21'b0000000_0000000_0000000;
	parameter WHITE = 21'b1111111_1111111_1111111; 
	parameter RED = 21'b1111111_0000000_0000000; 
	parameter GREEN = 21'b0000000_1111111_0000000; 	
	parameter BLUE = 21'b0000000_0000000_1111111;
	
	
	always@(posedge clk) 
	if (~bright)begin
		rgb = BLACK;
	end else if(((hCount >= 30) && (hCount <= 110)) && ((vCount >= 250) && (vCount <= 310)) && L[2] == 1)begin
		rgb = RED;
	end else if(((hCount >= 130) && (hCount <= 210)) && ((vCount >= 250) && (vCount <= 310)) && L[1] == 1)begin
		rgb = RED;
	end else if(((hCount >= 230) && (hCount <= 310)) && ((vCount >= 250) && (vCount <= 310)) && L[0] == 1)begin
		rgb = RED;	
	end else if(((hCount >= 330) && (hCount <= 410)) && ((vCount >= 250) && (vCount <= 310)) && R[0] == 1)begin
		rgb = RED;
	end else if(((hCount >= 430) && (hCount <= 510)) && ((vCount >= 250) && (vCount <= 310)) && R[1] == 1)begin
		rgb = RED;
	end else if(((hCount >= 530) && (hCount <= 610)) && ((vCount >= 250) && (vCount <= 310)) && R[2] == 1)begin
		rgb = RED;	
	end else begin
		rgb = BLUE;
	end 
	
	
	
	
endmodule 