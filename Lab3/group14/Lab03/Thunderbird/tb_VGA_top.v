module tb_VGA_top;
	reg clk, clear;
	reg left, right, haz; 
	wire [7:0] red, green, blue;
	wire hSync, vSync; 
	wire slowclk = 1'b0;
	integer i;
	
	VGA_top VAG (clk, clear, slowclk, red, green, blue, 
		hSync, vSync,left, right, haz);
	
	initial begin
	clk = 0;
	clear = 0;
	left = 0;
	right = 0;
	haz = 0;
	
	for (i = 0; i < 600; i = i + 1) begin
		clk = ~clk;
		if (i < 10)
			clear = 1;
		else
			clear = 0;
		
		if (i < 200) begin
			left = 1;
			right = 0;
			haz = 0;
		end	
		else if (i < 400) begin
			left = 0;
			right = 1;
			haz = 0;
		end
		else begin
			left = 0;
			right = 0;
			haz = 1;		
		end
		#5;
	end
	
	end 


endmodule 