module VGA_top (clk, clear, slowclk, red, green, blue, 
	hSync, vSync,left, right, haz);
	
	input clk, clear;
	input left, right, haz; 
	output wire [7:0] red, green, blue;
	output wire hSync, vSync; 
	wire bright;
	wire [9:0] hCount, vCount;
	output reg slowclk = 1'b0;
	wire [2:0] L, R;
	wire [20:0] rgb;
	
	VGA_Controller control(slowclk, clear, hSync, vSync, bright, hCount, vCount);
	VGA_Bitgen bitgen(bright, L, R, slowclk, hCount, vCount, rgb);
	t_bird1 bird (left, right, haz, ~clear, slowclk, L, R);	
	
	always@(posedge clk)
		begin
			slowclk <= ~slowclk;
		end

	
	assign red = bright ? rgb[20:14] : 7'b0000000;
	assign green = bright ? rgb[13:7] : 7'b0000000;
	assign blue = bright ? rgb[6:0] : 7'b0000000;
	
endmodule	
