module VGA_Controller(input wire clk,
input wire clear,
output reg hSync,
output reg vSync,
output reg bright,
output reg [10:0] hCount,
output reg [10:0] vCount
);


parameter TotalHorizontalPixels = 10'd800;
parameter HorizontalSyncWidth = 10'd96;
parameter VerticalSyncWidth = 10'd2;

parameter TotalVerticalLines = 10'd521;
parameter HorizontalBackPorchTime = 10'd144 ;
parameter HorizontalFrontPorchTime = 10'd784 ;
parameter VerticalBackPorchTime = 10'd31 ;
parameter VerticalFrontPorchTime = 10'd511;

reg VerticalSyncEnable;

reg [10:0] HorizontalCounter;
reg [10:0] VerticalCounter;

//Counter for the horizontal sync signal
always @(posedge clk)
begin
	if(clear == 1)
		HorizontalCounter <= 0;
	else
		begin
			if(HorizontalCounter == TotalHorizontalPixels - 1)
				begin //the counter has hreached the end of a horizontal line
					HorizontalCounter<=0;
					VerticalSyncEnable <= 1;
				end
			else
				begin 
					HorizontalCounter<=HorizontalCounter+1; 
					VerticalSyncEnable <=0;
				end
		end
end

//Generate the hsync pulse
//Horizontal Sync is low when HorizontalCounter is 0-127

always @(*)
begin
	if((HorizontalCounter<HorizontalSyncWidth))
		hSync = 0;
	else
		hSync = 1;
end

//Counter for the vertical sync

always @(posedge clk)
begin
	if(clear == 1)
		VerticalCounter<=0;
	else
	begin
		if(VerticalSyncEnable == 1)
			begin
				if(VerticalCounter==TotalVerticalLines-1)
					VerticalCounter<=0;
				else
					VerticalCounter<=VerticalCounter+1;
			end
	end
end

//generate the vsync pulse
always @(*)
begin
	if(VerticalCounter < VerticalSyncWidth)
		vSync = 0;
	else
		vSync = 1;
end

always @(posedge clk)
begin
	if((HorizontalCounter<HorizontalFrontPorchTime) && (HorizontalCounter>HorizontalBackPorchTime) && (VerticalCounter<VerticalFrontPorchTime) && (VerticalCounter>VerticalBackPorchTime))
		begin
			bright <= 1;
			hCount<= HorizontalCounter - HorizontalBackPorchTime;
			vCount<= VerticalCounter - VerticalBackPorchTime;
		end
	else
		begin
			bright <= 0;
			hCount<=0;
			vCount<=0;
		end
end

endmodule

					 