module t_bird1(left, right, haz, rst, clk, L, R);

input rst, clk;
input left, right, haz;

output reg [2:0] L, R;

reg [2:0] PS, NS;
reg [25:0] counter;

parameter [2:0] S0 = 3'b000, S1 = 3'b001, S2 = 3'b010, S3 = 3'b011, S4 = 3'b100,
S5 = 3'b101, S6 = 3'b110, S7 = 3'b111;

//sequential
always@(posedge clk, negedge rst) begin
	if(rst == 0) begin
	PS <= S0;// counter <= 0;
	end
	else if (counter == 50_000_000) begin 
	PS <= NS; 
	counter <= 0;
	end
	else begin
	counter <= counter + 1; 
	//PS <= NS;
	end
	
end

//combinational
always@* begin

	L = 3'b000; R = 3'b000;
	case(PS)
		S0: begin
					L = 3'b000; R = 3'b000;
					if(haz == 1) NS = S7;
					else if(left == 1) NS = S1;
					else if(right == 1) NS = S4;
					else NS = S0;
		end
		S1: begin
					L = 3'b001; R = 3'b000;
					if(haz == 1) NS = S7;
					else if(left == 1) NS = S2;
					//else if(right == 1) NS = S4;
					else NS = S0;

		end
		S2: begin
					L = 3'b011; R = 3'b000;
					if(haz == 1) NS = S7;
					else if(left == 1) NS = S3;
					//else if(right == 1) NS = S4;
					else NS = S0;
		
		end
		S3: begin
					L = 3'b111; R = 3'b000;
					if(haz == 1) NS = S7;
					else if(left == 1) NS = S0;
					//else if(right == 1) NS = S4;
					else NS = S0;	
	
		end
		S4: begin
					L = 3'b000; R = 3'b001;
					if(haz == 1) NS = S7;
					//else if(left == 1) NS = S1;
					else if(right == 1) NS = S5;
					else NS = S0;

		end
		S5: begin
					L = 3'b000; R = 3'b011;
					if(haz == 1) NS = S7;
					//else if(left == 1) NS = S1;
					else if(right == 1) NS = S6;
					else NS = S0;
	
		end
		S6: begin
					L = 3'b000; R = 3'b111;
					if(haz == 1) NS = S7;
					//else if(left == 1) NS = S1;
					else if(right == 1) NS = S0;
					else NS = S0;

		end
		S7: begin
					L = 3'b111; R = 3'b111;
					NS = S0;


		end
		default: begin
					L = 3'b000; R = 3'b000;
		end
	endcase
	

end

endmodule
