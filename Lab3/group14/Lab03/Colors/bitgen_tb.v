`timescale 1ns/1ps
/*
 * Colton Watson and Yuntong Lu
 * ECE 3710 Fall 2019
 * Simple testbench for the colors examples.
 */
module bitgen_tb;
	reg bright;
	reg [11:0] switches;
	
	wire [7:0] r, g, b;
	
	integer i;
	
	vga_bitgen UUT (.bright(bright), .switches(switches), .r(r), .g(g), .b(b));
	
	initial begin
	
		bright = 1'b0;
		switches = 8'b0;
		
		#10;
		
		for(i = 0; i < 2**13; i = i + 1) begin
			{switches, bright} = i;
			#10;
		end

	end
	
endmodule
