/*
 * Colton Watson and Yuntong Lu
 * ECE 3710 Fall 2019
 *
 * The bit generator for the varios colors. We added extra bits for more colors, about 4K.
 */
module bitgen(bright, switches, r, g, b);
	input bright;
	input [11:0] switches;
	
	output reg [7:0] r, g, b;
	
	always@(*) begin
		if (bright) begin
			r = {switches[0], switches[1],  switches[2],  switches[3], 4'b0000};
			g = {switches[4], switches[5],  switches[6],  switches[7], 4'b0000};
			b = {switches[8], switches[9],  ~switches[10], ~switches[11], 4'b0000}; // I tied the last two inputs to the buttons, they're active low.
		end
		else begin
			r = 8'd0;
			g = 8'd0;
			b = 8'd0;
		end
	
	end
	
	
	
	
	
	
endmodule
