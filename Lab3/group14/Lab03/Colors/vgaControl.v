/* 
 * Colton Watson, Yuntong Lu
 * ECE 3710 Lab 3 - VGA - Fall 2019
 * Simplified VGA controller.
 */

module vgaControl (clk, rst, hSync, vSync, vga_clk, vga_blank_n, bright, hCount, vCount); 
	input clk, rst;
	output reg bright, vga_blank_n, vga_clk;
	output hSync, vSync;
	output reg [9:0] hCount, vCount;
	
	
	parameter hs_start = 16;
	parameter hs_sync = 96;
	parameter hs_end = 48;
	parameter hs_total = 800;
	
	parameter vs_init = 480;
	parameter vs_start = 10;
	parameter vs_sync = 2;
	parameter vs_end = 33;
	parameter vs_total = 525;
	
	

	
	reg counter;
	
	
	always@(posedge clk) begin
	
		if(rst == 0) begin
			hCount <= 0;
			vCount <= 0;
			vga_clk <= 0;
			counter <= 0;
			
		end
		
		else if(counter == 1) begin
		
			hCount <= hCount + 1;
			if(hCount == hs_total) begin
					hCount <= 0;
					vCount <= vCount + 1;
					if(vCount == vs_total) vCount <= 0;
			end
		end
		
		counter <= counter + 1;
		vga_clk <= ~vga_clk;
		
		
	
	end
	
	assign hSync = ~((hCount >= hs_start) & (hCount < hs_start + hs_sync));
	assign vSync = ~((vCount >= vs_init + vs_start) & (vCount < vs_init+vs_start+vs_sync));
	
	
	always@* begin
	
	if(hCount >= 144 && hCount<784 && vCount < 480) begin
		vga_blank_n = 1;
		bright = 1;
		
	end
	
	else begin
	
		vga_blank_n = 0;
		bright = 0;
	
	end
	
	end
	
endmodule
	