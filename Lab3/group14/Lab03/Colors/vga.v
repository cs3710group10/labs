/*
 * Colton Watson and Yuntong Lu
 * ECE 3710 Fall 2019
 * Top module for the colors example.
 *
 */
module vga(clk, rst, switches, hSync, vSync, vga_clk, vga_blank_n, bright, hCount, vCount, r, g, b);
	input clk;
	input rst;
	input [11:0] switches;
	output hSync, vSync, vga_clk, vga_blank_n, bright;
	output [9:0] hCount, vCount;
	output [7:0] r, g, b;
	
	
	vgaControl cont (.clk(clk), .rst(rst), .hSync(hSync), .vSync(vSync), .vga_clk(vga_clk), .vga_blank_n(vga_blank_n), .bright(bright), .hCount(hCount), .vCount(vCount));
	bitgen gen(.bright(bright), .switches(switches), .r(r), .g(g), .b(b));
	


endmodule
