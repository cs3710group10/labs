# Modified fib.n
# Colton Watson
# ECE 3710
# Lab 2
fibn: addi $3, $0, 14    # n = 14
	  addi $4, $0,  1    # f1 = 1
	  addi $5, $0, -1    # f2 = 2
	  addi $6, $0, 128   # arr = 128
	  addi $7, $0, 128   # copy of adr
loop: beq $3, $0, end    # done with loop if n = 0
	  add $4, $4, $5     # f1 = f1 + f2
	  sub $5, $4, $5     # f2 = f1 - f2
	  addi $3, $3, -1    # n = n - 1
	  
	  sb   $4, 0($6)     # Mem[arr] <- f1
	  addi $6, $0, 1     # arr++
	  
	  j loop             # jump back to loop
	 
end:  lb $1 0($0)        # get value in switches
	  lb $2, 128($1)     # get the fibonacci value at the switches
	  sb $2  128($1)     # load the fibonacci value into the leds
	  
	  j end              # Keep getting input.