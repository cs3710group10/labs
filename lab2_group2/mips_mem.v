module mips_mem(input clk, reset, output memread);

	wire [7:0] memdata, writedata, adr;
	wire memwrite;
	
	wire en = 1;
	
	mips proc (.clk(clk), .reset(reset), .memdata(memdata), .memread(memread), .memwrite(memwrite), .adr(adr), .writedata(writedata));
	
	exmem mem (.clk(~clk), .en(en), .memwrite(memwrite), .adr(adr), .writedata(writedata), .memdata(memdata));


endmodule
