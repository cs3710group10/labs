`timescale 1ns / 1ps

module mips_tb;

// Inputs
reg clk;
reg reset;


//output
wire memread;

// Bidirs

// Instantiate the UUT
mips_mem UUT (.clk(clk), .reset(reset), .memread(memread));

// Initialize Inputs
initial begin
		reset <= 1;
		#22 reset <= 0;
end

// Generate clock to sequence tests
always 
begin
	clk <= 1; #5; clk <= 0; #5;
end

// Check the data on the memory interface between mips and exmem
// If you're writing, and the address is 255, then the data should 
// be 8'h0d if you've computed the correct 8th Fibonacci number
/*always@(negedge clk)
	if (UUT.memwrite)
		if (UUT.adr == 8'd255 & UUT.writedata == 8'h0d)
			$display("Fibonacci Succesful!");
			else $display("Wrong value written to addr 255: %h", UUT.writedata);
*/			
endmodule
